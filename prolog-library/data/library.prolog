%% library of general purpose functions
% For an introduction to predicate description notation, see chapter 4.1 of the
% SWIProlog manual (http://www.swi-prolog.org/pldoc/man?section=preddesc) or
% similar source.

%% Resource quantities.
money(Amount):- player(Player), Amount is Player.cash.
buzz(Amount):- context(Ctx), Amount is Ctx.sim.player.gettokens($tokentype.buzz).
influence(Amount):- context(Ctx), Amount is Ctx.sim.player.gettokens($tokentype.favors).
prestige(Amount):- context(Ctx), Amount is Ctx.sim.prestige.getprestigecurrent().


%% Types of entities.

%% office(++Entity)
office(Entity):- config_named(Entity, "office", O), O \= null.
%% apartment(++Entity)
apartment(Entity):- config_named(Entity, "residence", R), R \= null.
%% restaurant(++Entity)
restaurant(Entity):- service_type(Entity, "Restaurant").
%% retail(++Entity)
retail(Entity):- service_type(Entity, "Retail").
%% service(++Entity)
service(Entity):- service_type(Entity, "Service").


%% Utility meters.

%% power_meter(++Entity)
power_meter(Entity):- is_meter(Entity, "UtilPower").
%% telephone_meter(++Entity)
telephone_meter(Entity):- is_meter(Entity, "UtilWater").
%% cable_meter(++Entity)
cable_meter(Entity):- is_meter(Entity, "UtilCable").
%% water_meter(++Entity)
water_meter(Entity):- is_meter(Entity, "UtilWater").
%% gas_meter(++Entity)
gas_meter(Entity):- is_meter(Entity, "UtilGas").
%% hvac_meter(++Entity)
hvac_meter(Entity):- is_meter(Entity, "UtilHVAC").


%% Hooks, etc.

%% subscribe(++PredicateName)
% PredicateName is the String name of a single-arity predicate
subscribe(PredicateName):- adaprolog(ADA), ADA.subscribehook(PredicateName).

% TODO add unsubscribe?
