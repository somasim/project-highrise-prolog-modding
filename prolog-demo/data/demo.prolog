% example prolog file for testing magic ada :)

%% replacing values
onValueChanged(Value, Value).

%% formatting responses
onEndEdit(Question, Response) :- get_response(Question, Response).

get_response(Question, Response):-
	word_list(Question,Words),
	question(Type,Category,Words, []),
	entity_count(Type,Category,Count),
	word_list(Response, ["you", "have", Count, Type]).


get_response("Hello", "Hello, I'm ADA.").
get_response(_, "I don't understand."). %% generic response

%% grammar

%% why are people unhappy?
%% how many offices do I have?
%% how much money do I have?
%% what services am I lacking?
%% tell me about utility capacity

question(Type, Category) -->
	[how, many],
	typephrase(Type),
	[do, i, have].

typephrase(Type) --> ({ lex(Type,Category) }, [Type]).

%% lexicon
lex(services, tenant).
lex(offices, tenant).
lex(restaurants, tenant).
lex(apartments, tenant).
lex(shops, tenant).

lex(power, utility).
lex(phone, utility).
lex(cable, utility).
lex(water, utility).
lex(gas, utility).
lex(hvac, utility).
