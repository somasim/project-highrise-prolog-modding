%% Utility library for use in Prolog modding of Project Highrise.
getunit(Entity):- allunits(U), member(Entity, U).

%% context(-Ctx)
context(Ctx):- property($"Game", "ctx", Ctx).
%% sim(-Sim)
sim(Sim):- context(C), property(C, "sim", Sim).
%% player(-Player)
player(Player):- sim(S), property(S, "player", Player).

%% serv(-Serv)
serv(Serv):- property($"Game", "serv",Serv).
%% settings(-Settings)
settings(Settings):- property($"Game", "settings", Settings).


%% entityman(-EMan)
entityman(EMan):- context(C), property(C, "entityman", EMan).

%% allunits(-Units), 
allunits(Units):- entityman(EMan), Units is EMan.makelistofallunits().
%% allpeeps(-Peeps)
allpeeps(Peeps):- entityman(EMan), Peeps is EMan.makelistofallpeeps().

%% template(+Name, -Template)
template(Name, Template):-
	nonvar(Name),
	entityman(EMan),
	Template is EMan.findtemplate(Name).

%% config_named(+Entity, +ConfigName, -Config)
config_named(Entity, ConfigName, Config):-
	subelement(Entity, "config", ConfigName, Config).

%% component_named(+Entity, +ComponentName, -Component)
component_named(Entity, ComponentName, Component):-
	subelement(Entity, "component", ComponentName, Component).

%% data_named(+Entity, +DataName, -Data)
data_named(Entity, DataName, Data):-
	subelement(Entity, "data", DataName, Data).

subelement(Entity, ElementName, SubelementName, Subelement):-
	nonvar(Entity), nonvar(ElementName), nonvar(SubelementName),
	property(Entity, ElementName, Element),
	Element \= null,
	property(Element, SubelementName, Subelement).


%% adaprolog herself
%% adaprolog(-ADA)
adaprolog(ADA):- context(Ctx), property(Ctx, "adaprolog", ADA).





sizeof(Entity, [X, Y]):- 
	config_named(Entity,"placement",Placement),
	property(Placement,"size",Size),
	property(Size,"x",X), % yikes
	property(Size,"y",Y).


service_type(Entity, TypeName):-
	config_named(Entity, "service", Service),
	Service \= null,
	property(Service, "servicetype", Type),
	string_representation(Type, TypeName).

is_meter(Entity, Type):-
	config_named(Entity, "closet", Closet),
	Closet\=null,
	property(Closet, "provides", Provides),
	member(X,Provides),
	property(X,"type", T),
	string_representation(T, Type).