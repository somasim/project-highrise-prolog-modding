predicates([
	context, sim, player, serv, settings,
	money, buzz, influence, prestige,
	office, apartment, retail, restaurant, service,
	power_meter, telephone_meter, cable_meter, gas_meter, hvac_meter, water_meter]).

problems_with(L):- findall(X, (predicates(Preds), member(X, Preds), \+verify(X)), L).

verify(context):- context(_).
verify(sim):- sim(_).
verify(player):- player(_).
verify(serv):- serv(_).
verify(settings):- settings(_).

% resources
verify(money):- resource_check(money).
verify(buzz):- resource_check(buzz).
verify(influence):- resource_check(influence).
verify(prestige):- resource_check(prestige).
verify(horse):- resource_check(horse).

resource_check(X):- call(X, _).

verify(office):- unit_check(office).
verify(apartment):- unit_check(apartment).
verify(retail):- unit_check(retail).
verify(restaurant):- unit_check(restaurant).
verify(service):- unit_check(service).
verify(power_meter):- unit_check(power_meter).
verify(telephone_meter):- unit_check(telephone_meter).
verify(cable_meter):- unit_check(cable_meter).
verify(gas_meter):- unit_check(gas_meter).
verify(hvac_meter):- unit_check(hvac_meter).
verify(water_meter):- unit_check(water_meter).

unit_check(X):- getunit(U), call(X, U).